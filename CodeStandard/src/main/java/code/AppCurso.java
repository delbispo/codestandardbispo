package code;

public class AppCurso {
    public static void main( String[] args ){

        Person jaum = new Person();
        jaum.setAge(20);
        jaum.setName("João");

        System.out.println( "Oi, eu sou o" + jaum.getName() + ", e tenho " + jaum.getAge() + " anos." );
    }
}
